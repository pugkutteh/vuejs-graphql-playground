# VueJS + GraphQL Playground

Hi there! This is my first attempt at building a web app using Laravel, VueJS and GraphQL. Do leave a feedback and help me to improve for the better :)

---

## Stack Used

-   Laravel 8
-   Vue 2, Vuetify, Vue Router
-   GraphQL

## // TODO:

-  Vuex?
-  Vue 3?
-  Unit tests? 🙃