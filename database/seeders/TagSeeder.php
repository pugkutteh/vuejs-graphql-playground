<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = [
            ['id' => 1, 'title' => 'Adobe XD'],
            ['id' => 2, 'title' => 'Binance Websocket'],
            ['id' => 3, 'title' => 'Bootstrap'],
            ['id' => 4, 'title' => 'Conceptual'],
            ['id' => 5, 'title' => 'Desktop App'],
            ['id' => 6, 'title' => 'Electron'],
            ['id' => 7, 'title' => 'Flutter'],
            ['id' => 8, 'title' => 'Ghost CMS'],
            ['id' => 9, 'title' => 'Ionic'],
            ['id' => 10, 'title' => 'Laravel'],
            ['id' => 11, 'title' => 'Mobile App'],
            ['id' => 12, 'title' => 'React'],
            ['id' => 13, 'title' => 'Web App'],
            ['id' => 14, 'title' => 'WIP'],
        ];

        foreach($tags as $tag) {
            \DB::table('tags')->insert($tag);
        }
    }
}
