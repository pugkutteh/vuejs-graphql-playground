<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projects = [
            [
                'slug' => 'crypto-trades',
                'title' => 'Crypto Trades',
                'media_type' => 'video',
                'media_filename' => 'crypto-trades.webm',
                'background_color' => '#303030',
            ],
            [
                'slug' => 'crypto-arbitrage',
                'title' => 'Crypto Arbitrage',
                'media_type' => 'image',
                'media_filename' => 'crypto-arbitrage.jpg',
                'background_color' => '#e5e5e5',
            ],
            [
                'slug' => 'logit',
                'title' => 'Logit',
                'media_type' => 'image',
                'media_filename' => 'logit.jpg',
                'background_color' => '#e5e5e5',
            ],
            [
                'slug' => 'payfinn',
                'title' => 'Payfinn',
                'media_type' => 'video',
                'media_filename' => 'payfinn.webm',
                'background_color' => '#ffffff',
            ],
            [
                'slug' => 'agile-scada',
                'title' => 'Agile SCADA',
                'media_type' => 'image',
                'media_filename' => 'agile-scada.png',
            ],
            [
                'slug' => 'linkoy',
                'title' => 'Linkoy',
                'media_type' => 'image',
                'media_filename' => 'linkoy.png',
            ],
            [
                'slug' => 'propfolio',
                'title' => 'Propfolio',
                'media_type' => 'image',
                'media_filename' => 'propfolio.jpg',
                'background_color' => '#e5e5e5',
            ],
        ];

        foreach($projects as $project) {
            \DB::table('projects')->insert($project);
        }
    }
}
