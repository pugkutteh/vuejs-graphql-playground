<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// TODO: GraphQL
Route::resource('users', UserController::class);

Route::get('/graphql', function() {
    return redirect('/404');
});
Route::get('/{any?}', [AppController::class, 'index'])->where('any', '.*');