export default [
    {
        path: '/',
        redirect: '/projects',
    },
    {
        path: '/projects',
        name: 'projects',
        component: require('./projects/Index.vue').default,
        meta: { title: 'My Projects' },
    },
    {
        path: '/projects/:slug',
        name: 'project.details',
        component: require('./project-details/Index.vue').default,
        props: true,
    },
    {
        path: '/enjin-users',
        name: 'enjin.users',
        component: require('./enjin-users/Index.vue').default,
        meta: { title: 'Enjin Users' },
    },
    {
        path: '*',
        name: '404',
        component: require('./PageNotFound.vue').default,
        meta: { title: 'Oops!' },
    }
]