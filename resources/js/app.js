import '@mdi/font/css/materialdesignicons.css';
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import routes from './routes';

Vue.use(VueRouter);
Vue.use(Vuetify);

// global components
Vue.component('main-layout', require('./layouts/MainLayout.vue').default);

// router
const router = new VueRouter({
    mode: 'history',
    routes,
});
router.beforeEach((to, from, next) => {
    window.document.title = to.meta && to.meta.title ? to.meta.title + ' - Playground' : 'Playground';
    next();
});

// create root instance
const app = new Vue({
    el: '#app',
    vuetify: new Vuetify(),
    router,
});
