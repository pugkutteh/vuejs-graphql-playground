<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function index() {
        $accessToken = Session::get('appAccessToken');

        $response = Http::withHeaders([
            'Authorization' => "Bearer $accessToken",
        ])->post(env('ENJIN_TESTNET_ENDPOINT'), [
            'query' => 'query {
                EnjinApp {
                    name
                    identities {
                        user {
                            id
                            name
                            createdAt
                        }
                    }
                }
            }',
            'variables' => [
                'id' => env('ENJIN_APP_ID'),
                'secret' => env('ENJIN_APP_SECRET'),
            ],
        ]);
        $response = $response->json();

        // format response
        $data = [
            'projectName' => '',
            'users' => [],
        ];
        if (!array_key_exists('errors', $response)) {
            $result = $response['data']['EnjinApp'];
            $projectName = $result['name'];

            $data['projectName'] = $projectName;
            $data['users'] = array_map(function($identity) use ($projectName) {
                return [
                    'id' => $identity['user']['id'],
                    'name' => $identity['user']['name'],
                    'project' => $projectName,
                    'createdTs' => $identity['user']['createdAt'],
                ];
            }, $result['identities']);
        }
        
        return response()->json($data);
    }
    
    public function store(Request $request) {
        $accessToken = Session::get('appAccessToken');

        $response = Http::withHeaders([
            'Authorization' => "Bearer $accessToken",
        ])->post(env('ENJIN_TESTNET_ENDPOINT'), [
            'query' => 'mutation CreateEnjinUser($name: String!) {
                CreateEnjinUser(name: $name) {
                    id
                    name
                    accessTokens
                }
            }',
            'variables' => [
                'name' => $request->name,
            ],
        ]);
        $response = $response->json();

        // format response
        $data = [];
        if (!array_key_exists('errors', $response)) {
            return response()->json(['name' => $request->name]);
        } else {
            $errorMessage = 'Something went wrong. Please try again.';
            $errorCode = $response['errors'][0]['code'];

            $validationError = $response['errors'][0]['validation'] ?? null;
            if (isset($validationError)) {
                $errorKey = array_key_first($validationError);
                $errorMessage = $validationError[$errorKey][0] ?? $errorMessage;
            }

            return response()->json(['error' => $errorMessage], $errorCode);
        }
    }
}
