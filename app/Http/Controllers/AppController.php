<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class AppController extends Controller
{
    public function index() {
        $response = Http::post(env('ENJIN_TESTNET_ENDPOINT'), [
            'query' => 'query AuthApp($id: Int!, $secret: String!) {
                AuthApp(id: $id, secret: $secret) {
                    accessToken
                }
            }',
            'variables' => [
                'id' => env('ENJIN_APP_ID'),
                'secret' => env('ENJIN_APP_SECRET'),
            ],
        ]);
        $result = $response->json();

        Session::put('appAccessToken', $result['data']['AuthApp']['accessToken'] ?? '');
        
        return view('app');
    }
}
