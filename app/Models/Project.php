<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $with = ['tags'];
    protected $hidden = ['pivot'];
    
    public function tags() {
        return $this->belongsToMany(Tag::Class, 'project_tags');
    }
}
